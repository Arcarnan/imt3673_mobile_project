# imt3673-project
Done by:
	Christian Bråthen Tverberg - 473185.
	Maarten Dijkstra           - 473201.
	Nataniel Gåsøy             - 131390.


### The project
The final hand-in and project for the IMT3673 course, mobile development. 
In this task, we are trying to make a Pong game using P2P in order to have two mobiles connect through wifi direct in order to play a match against each other.
We will also include other functionality in the project.

Link to google docs: https://docs.google.com/document/d/1QYWujjgnst9CbXsfdJ33yYnrNuyyE4tH1r7izu042Pg/edit#heading=h.i9oyk06712dh


### Functionality
Core functionality: 
	- P2P, wifi direct.


Extra functionality:
	- Pong, moving player paddle using sensors (Accelerometer + magnetic field).
	- Database interaction, Firebase (highscore).
	- Localization (en/no), editable in settings menu.
	- Dark mode, toggle in settings menu.
	- Username, device-specific. Only entered the first time the application is started.
	- Font, 8-bit using a ttf file (unable to use 'Æ', 'Ø', 'Å').
	- P2P Chat, mainly used for debugging/ development.
	- Text-to-speech, in chat. Mainly there as a fun gimmick (entertainment purposes).
	- Animation, fluff. Confetti on main menu where screen is touched (particle effect).
	- Animation, smooth transitions between activities and setting changes. 
	- Animation, button changes colour and have a ripple animation effect when pressed.
	

Todo/ under consideration:
	- Multitask support.
	- Unit tests - Decided against doing this, as this would lead to a lot of unnecessary code.
	- Notifications - Decided against doing this, as we have no need of notifications. 
	
