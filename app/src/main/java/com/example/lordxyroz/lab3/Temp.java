package com.example.lordxyroz.lab3;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class Temp extends AppCompatActivity implements WifiP2pManager.PeerListListener {

    private ListView listView;

    private IntentFilter intentFilter = new IntentFilter();
    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private P2PBroadcastReceiver receiver;
    private List<WifiP2pDevice> peers = new ArrayList<>();

    public boolean stop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        listView = findViewById(R.id.temp_layout);

        // P2P stuff
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);

        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(Temp.this, "Starting discovering", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int reasonCode) {
                Toast.makeText(Temp.this, "Failed discovering " +  reasonCode, Toast.LENGTH_SHORT).show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                receiver.connect(peers.get(pos));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new P2PBroadcastReceiver(mManager, mChannel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList list) {
        List<WifiP2pDevice> refreshedPeers = new ArrayList(list.getDeviceList());

        if (!refreshedPeers.equals(peers)) {
            peers.clear();
            peers.addAll(refreshedPeers);

            ArrayAdapter<WifiP2pDevice> adapter = new ArrayAdapter<>(Temp.this, android.R.layout.simple_list_item_1, peers);
            listView.setAdapter(adapter);
            Toast.makeText(Temp.this, "Found devices!", Toast.LENGTH_SHORT).show();
            Log.d("P2PDEBUG", "Devices found!");
        }
        if (peers.size() == 0) {
            Toast.makeText(Temp.this, "No devices", Toast.LENGTH_SHORT).show();
            Log.d("P2PDEBUG", "No devices found");
        }
    }

    public void play(final InetAddress addr, Boolean client) {
        Intent intent;
        if (getIntent().getBooleanExtra("chat", false)) {
            intent = new Intent(this, DebugChatActivity.class);
        }
        else {
            intent = new Intent(this, GameActivity.class);
        }

        intent.putExtra("address", addr.getHostAddress());
        intent.putExtra("isClient", client);

        Log.e("Launching activity", "Launching the activity with intent");
        startActivity(intent);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        receiver.disconnect();
        stop = true;
        finish();
    }

}
