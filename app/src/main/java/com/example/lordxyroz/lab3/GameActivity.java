package com.example.lordxyroz.lab3;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.plattysoft.leonids.ParticleSystem;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

public class GameActivity extends AppCompatActivity implements SensorEventListener {

	private final String PREF_DARK_THEME = "darmodeState";
    private static final String PREF_NAME = "name";
    private static final String USER_PREFS = "usernamePreference";

    SharedPreferences mSharedPreferences;

	private boolean isRunning = false;

	private SurfaceHolder holder;
	private Vibrator haptic;
	private ToneGenerator sound;

	private SurfaceView surfaceView;


	private float display_size_x;
	private float display_size_y;

	private Player player;
	private Opponent opponent;
	private Ball ball;

	private SensorManager sensorManager;

	private float accelerometer[] = new float[3];
	private float magnetic[] = new float[3];
	private float rotation[] = new float[9];
	private float radians[] = new float[3];

	private double max_speed = 15;
	private double last_update = 0.d;
	private double gravity = 9.81d;

	private int opponentScore;
	private int playerScore;
	private boolean scored = false;

	float density;
	float scalex;
	float scaley;

    private darkmodeStartupClass darkmode;
    private boolean dark;

    private Runnable gameLogicRunnable;
    private Thread gameLogic;

    private AlertDialog.Builder alert;

    /////////////////////////////////////////////
    // P2P

    private DatagramSocket socket;
    final int port = 9999;
    InetAddress host = null;

    boolean isClient;

    DatagramPacket receivePacket;

    byte receiveData[] = new byte[512];
    byte sendData[] = new byte[512];

    private Runnable runnable;
    private Thread thread;
    /////////////////////////////////////////////


	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    // There is a bug that creates several instances of the activity that repeatedly created/destroyed        //
        // The reason for it is screen orientation. The fix is to set the orientation in AndroidManifest.xml file //
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        getWindow().setFlags(0xFFFFFFFF, WindowManager.LayoutParams.FLAG_FULLSCREEN| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Log.e("ONCREATE", "oncreate was called");
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(GameActivity.this, PREF_DARK_THEME);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(GameActivity.this);
        dark = prefs.getBoolean(PREF_DARK_THEME, false);

        super.onCreate(savedInstanceState);

		sensorManager = (SensorManager) GameActivity.this.getSystemService(Context.SENSOR_SERVICE);


		surfaceView = new SurfaceView(GameActivity.this);
		holder = surfaceView.getHolder();
		setContentView(surfaceView);



		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		display_size_x = size.x;
		display_size_y = size.y;
		density = getResources().getDisplayMetrics().density;


		haptic = (Vibrator) GameActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
		sound = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);


		player = new Player();
		opponent = new Opponent();
		ball = new Ball();

        alert = new AlertDialog.Builder(GameActivity.this)
                .setTitle("Score!")
                .setMessage("Player: " + Integer.toString(playerScore) + "\nOpponent: " + Integer.toString(opponentScore))
                .setNegativeButton( R.string.textOk, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("BUTTON IS PUSHED", "THE OK BUTTON GOT PUSHED!");
                        scored = false;
                    }
                });

        alert.create();

		GameActivity.this.isClient = getIntent().getBooleanExtra("isClient", false);
		try {
		    if (isClient) host = InetAddress.getByName(getIntent().getStringExtra("address"));
        }
        catch (IOException exc) {
		    Log.e("HOST ERROR", exc.getMessage());
        }

        final boolean temp = isClient;

        runnable = new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    if (temp) {
                        try {
                            if (socket == null) {
                                socket = new DatagramSocket(port);
                                socket.setSoTimeout(5);
                                socket.connect(host, port);

                                DatagramPacket temp = new DatagramPacket(new byte[12], new byte[12].length, host, port);
                                socket.send(temp);
                            }
                        }
                        catch (IOException exc) {
                            Log.e("P2P client socket", exc.getMessage());
                        }

                        DatagramPacket sendPackage = new DatagramPacket(sendData, sendData.length, host, port);
                        try {
                            if (host != null)
                                if (socket != null)
                                    socket.send(sendPackage);
                        } catch (IOException exc) {
                            //Log.e("Send exception", exc.getMessage());
                        }

                        try {
                            Thread.sleep(1);
                        }
                        catch (InterruptedException exc) {
                            Log.e("Loop ending", "Thanks to exception");
                            break;
                        }

                        try {
                            receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            socket.receive(receivePacket);
                        }
                        catch (IOException exc) {
                            //Log.e("P2P client receive", exc.getMessage());
                        }
                        catch (NullPointerException exc) {
                            Log.e("P2P client receive nullptr", exc.getMessage());
                        }
                    }
                    else {
                        try {
                            if (socket == null) {
                                socket = new DatagramSocket(port);
                                socket.setSoTimeout(5);
                            }
                        }
                        catch (IOException exc) {
                            Log.e("P2P host socket", exc.getMessage());
                        }

                        try {
                            receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            socket.receive(receivePacket);

                            if (host == null) {
                                host = receivePacket.getAddress();
                                socket.connect(host, port);
                                DatagramPacket temp = new DatagramPacket(new byte[12], new byte[12].length, host, port);
                                socket.send(temp);
                            }
                        }
                        catch (IOException exc) {
                            //Log.e("P2P host receive", exc.getMessage());
                        }
                        catch (NullPointerException exc) {
                            Log.e("P2P host receive nullptr", exc.getMessage());
                        }

                        DatagramPacket sendPackage = new DatagramPacket(sendData, sendData.length, host, port);
                        try {
                            if (host != null)
                                if (socket != null)
                                    socket.send(sendPackage);
                        } catch (IOException exc) {
                            //Log.e("Send exception", exc.getMessage());
                        }

                        try {
                            Thread.sleep(1);
                        }
                        catch (InterruptedException exc) {
                            Log.e("Loop ending", "Thanks to exception");
                            break;
                        }
                    }
                }
                //Log.e("AAAAAAA", "AAAAAAAAAAAAA");
            }
        };

        gameLogicRunnable = new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    double now = System.nanoTime() / 10000000.d;
                    double delta_time = now - last_update;

                    int temp = (receiveData[13] << 24) & 0xff000000 |
                            (receiveData[14] << 16) & 0x00ff0000 |
                            (receiveData[15] << 8) & 0x0000ff00 |
                            (receiveData[16] << 0) & 0x000000ff;

                    if (isClient) delta_time = temp / 1000.d;


                    if (delta_time > 1 / 120.d) {

                        int dt = (int) delta_time * 1000;
                        if (!scored) {

                            player.update(dt / 1000.d);
                            opponent.update(dt / 1000.d);
                            ball.update();


                            Canvas canvas = holder.lockCanvas();
                            if (canvas != null) {
                                if (dark) canvas.drawColor(Color.DKGRAY);
                                else canvas.drawColor(Color.LTGRAY);

                                player.draw(canvas);
                                opponent.draw(canvas);
                                ball.draw(canvas);
                                holder.unlockCanvasAndPost(canvas);
                            }
                        }

                        if (!isClient) {

                            sendData[13] = (byte) (dt >> 24);
                            sendData[14] = (byte) (dt >> 16);
                            sendData[15] = (byte) (dt >> 8);
                            sendData[16] = (byte) (dt >> 0);

                            sendData[25] = (byte) (scored ? 1 : 0);
                            sendData[26] = (byte) (playerScore);
                            sendData[27] = (byte) (opponentScore);

                        } else {
                            temp = (sendData[25]) & 0x000000ff;
                            scored = temp != 0;
                            playerScore = (sendData[26]) & 0x000000ff;
                            opponentScore = (sendData[27]) & 0x000000ff;
                        }

                        int tempvar = (int) (display_size_x);

                        sendData[17] = (byte) (tempvar >> 24);
                        sendData[18] = (byte) (tempvar >> 16);
                        sendData[19] = (byte) (tempvar >> 8);
                        sendData[20] = (byte) (tempvar >> 0);

                        tempvar = (int) (display_size_y);

                        sendData[21] = (byte) (tempvar >> 24);
                        sendData[22] = (byte) (tempvar >> 16);
                        sendData[23] = (byte) (tempvar >> 8);
                        sendData[24] = (byte) (tempvar >> 0);

                        int dens = (receiveData[17] << 24) & 0xff000000 |
                                (receiveData[18] << 16) & 0x00ff0000 |
                                (receiveData[19] << 8) & 0x0000ff00 |
                                (receiveData[20] << 0) & 0x000000ff;

                        scalex = display_size_x / dens;

                        dens = (receiveData[21] << 24) & 0xff000000 |
                                (receiveData[22] << 16) & 0x00ff0000 |
                                (receiveData[23] << 8) & 0x0000ff00 |
                                (receiveData[24] << 0) & 0x000000ff;

                        scaley = display_size_y / dens;

                    }

                    last_update = now;

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException exc) {
                        Log.e("Loop ending", "Thanks to exception");
                        break;
                    }

                    if (scored) {
                        player.reset();
                        opponent.reset();
                        ball.reset();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                (GameActivity.this).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(GameActivity.this, "Player: " + playerScore + "\n Opponent: " + opponentScore, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                //alert.show();
                            }
                        });
                        scored = false;
                    }
                }
            }
        };

		thread = new Thread(runnable);
		gameLogic = new Thread(gameLogicRunnable);
	}

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ONRESUME", "onresume was called");

        thread.start();
        gameLogic.start();

        sensorManager.registerListener(GameActivity.this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(GameActivity.this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
    }


	@Override
    public void onBackPressed() {
	    socket.close();
	    try {
	        thread.interrupt();
	        gameLogic.interrupt();
	        thread.join();
	        gameLogic.join();
        }
        catch (InterruptedException exc) {
	        Log.e("Error joining threads", exc.getMessage());
        }
        Log.e("Threads joined", "OK");
        sensorManager.unregisterListener(GameActivity.this);
        finish();
    }

    private void saveHighscore() {

        // Check who won the game:
        if (playerScore > opponentScore) {

            mSharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);
            String name = mSharedPreferences.getString(PREF_NAME, "");

            Map<String, Object> user = new HashMap<>();
            user.put("name", name);
            user.put("score", playerScore);

            FirebaseFirestore.getInstance().collection("highscores")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("highscore", "testing sending a highscore");
                        }
                    });
        }
    }

    @Override
    public void onStop() {

	    //Save highscore when game is finished.
	    saveHighscore();

        Log.e("ONSTOP", "onStop was called");
        Log.e("ONSTOP", Boolean.toString(isFinishing()));
        socket.close();
        try {
            thread.interrupt();
            gameLogic.interrupt();
            thread.join();
            gameLogic.join();
        }
        catch (Exception exc) {

        }
        super.onStop();
        finish();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometer[0] = sensorEvent.values[0];
            accelerometer[1] = sensorEvent.values[1];
            accelerometer[2] = sensorEvent.values[2];
        }
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetic[0] = sensorEvent.values[0];
            magnetic[1] = sensorEvent.values[1];
            magnetic[2] = sensorEvent.values[2];
        }

        SensorManager.getRotationMatrix(rotation, null, accelerometer, magnetic);
        SensorManager.getOrientation(rotation, radians);
    }




	public class Player {
	    private ShapeDrawable paddle;

	    private int x;
	    private int y;
	    private int d = (int) (25 * density);

	    private double y_vel;

	    Player() {
            paddle = new ShapeDrawable(new RectShape());
            paddle.getPaint().setColor(Color.RED);

            x = (int) (display_size_x - (50 * density));
            y = (int) (display_size_y / 2 - (d / (2 * density)));

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
        }

        void update(double dt) {
            y_vel = y_vel + (Math.sin(radians[2]) * gravity * dt);

            if (y_vel > max_speed) y_vel = max_speed;
            if (y_vel < -max_speed) y_vel = -max_speed;

            if (y - y_vel <= (d / 4) + (d * 2)|| y - y_vel >= display_size_y - (d * 2) - (d / 4)){
                y_vel = 0;
            }

            if (y_vel < 0.001 && y_vel > -0.001) y_vel = 0;

            y -= y_vel;

            sendData[0] = (byte) (y >> 24);
            sendData[1] = (byte) (y >> 16);
            sendData[2] = (byte) (y >>  8);
            sendData[3] = (byte) (y >>  0);

        }
        void draw (Canvas canvas) {
            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
            paddle.draw(canvas);
        }

        void reset() {
            x = (int) (display_size_x - (50 * density));
            y = (int) (display_size_y / 2 - (d / (2 * density)));
        }

    }

    public class Opponent {
        private ShapeDrawable paddle;

        private int x;
        private int y;
        private int d = (int) (25 * density);

        private double y_vel;

        Opponent() {
            paddle = new ShapeDrawable(new RectShape());
            paddle.getPaint().setColor(Color.RED);

            x = (int) (50 * density);
            y = (int) (display_size_y / (2 * density) - (d / (2 * density)));

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
        }

        void update(double dt) {
            if (receivePacket != null)
                receiveData = receivePacket.getData();

            y = (receiveData[0] << 24) &0xff000000|
                (receiveData[1] << 16) &0x00ff0000|
                (receiveData[2] <<  8) &0x0000ff00|
                (receiveData[3] <<  0) &0x000000ff;

            y = (int) (display_size_y - (y * scaley));

        }
        void draw (Canvas canvas) {
            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
            paddle.draw(canvas);
        }

        void reset() {
            x = (int) (50 * density);
            y = (int) (display_size_y / (2 * density) - (d / (2 * density)));
        }
    }

    public class Ball {
        private ShapeDrawable ball;

        private int x;
        private int y;
        private int d = (int) (25 * density);

        private double x_vel;
        private double y_vel;

        public Ball() {
            ball = new ShapeDrawable(new OvalShape());
            ball.getPaint().setColor(Color.RED);

            x = (int) display_size_x / 2 - d / 2;
            y = (int) display_size_y / 2 - d / 2;

            ball.setBounds(x - (d / 2), y - (d / 2), x + (d / 2), y + (d / 2));

            int rand1 = (int) Math.floor(Math.random() * 3);
            int rand2 = (int) Math.floor(Math.random() * 3);
            switch (rand1) {
                default:
                    x_vel = max_speed / 1.5;
                    break;
                case 0:
                    x_vel = max_speed / 1.5;
                    break;
                case 1:
                    x_vel = -max_speed / 1.5;
                    break;
            }
            switch (rand2) {
                default:
                    y_vel = max_speed / 1.5;
                    break;
                case 0:
                    y_vel = max_speed / 1.5;
                    break;
                case 1:
                    y_vel = -max_speed / 1.5;
                    break;
            }
        }

        public void update() {
            if (!isClient) {
                if (x_vel > max_speed * 1.5 / 2) x_vel = max_speed * 1.5 / 2;
                if (x_vel < -max_speed * 1.5 / 2) x_vel = -max_speed * 1.5 / 2;
                if (y_vel > max_speed * 1.5 / 2) y_vel = max_speed * 1.5 / 2;
                if (y_vel < -max_speed * 1.5 / 2) y_vel = -max_speed * 1.5 / 2;

                // TODO: Real pong collision
                if (x + (d / 2) >= player.x - (d / 4)) {
                    if (y >= player.y - (d * 2) && y <= player.y + (d * 2)) {
                        x_vel = -x_vel;
                    }
                }

                if (x - (d / 2) <= opponent.x + (d / 4)) {
                    if (y >= opponent.y - (d * 2) && y <= opponent.y + (d * 2)) {
                        x_vel = -x_vel;
                    }
                }

                // Bounce
                if (x - x_vel <= (d / 2)) {
                    playerScore++;
                    scored = true;
                }

                if (x - x_vel >= display_size_x - (d / 2)) {
                    opponentScore++;
                    scored = true;
                }

                if (y - y_vel <= (d / 2) || y - y_vel >= display_size_y - (d / 2)) {
                    y_vel = -y_vel * 1.1d;
                }

                sendData[4] = (byte) (x >> 24);
                sendData[5] = (byte) (x >> 16);
                sendData[6] = (byte) (x >>  8);
                sendData[7] = (byte) (x >>  0);

                sendData[8] = (byte) (y >> 24);
                sendData[9] = (byte) (y >> 16);
                sendData[10] = (byte) (y >>  8);
                sendData[11] = (byte) (y >>  0);

                x -= x_vel;
                y -= y_vel;

            }
            else {
                if (receivePacket != null)
                    receiveData = receivePacket.getData();

                x = (receiveData[4] << 24) & 0xff000000 |
                    (receiveData[5] << 16) & 0x00ff0000 |
                    (receiveData[6] << 8) & 0x0000ff00 |
                    (receiveData[7] << 0) & 0x000000ff;

                y = (receiveData[8] << 24) &0xff000000|
                    (receiveData[9] << 16) &0x00ff0000|
                    (receiveData[10] <<  8) &0x0000ff00|
                    (receiveData[11] <<  0) &0x000000ff;

                x = (int) ((display_size_x) - x * scalex);
                y = (int) ((display_size_y) - y * scaley);
            }

        }


        public void draw(Canvas canvas) {
            ball.setBounds(x - (d / 2), y - (d / 2), x + (d / 2), y + (d / 2));
            ball.draw(canvas);
        }

        public void reset() {
            x = (int) display_size_x / 2 - d / 2;
            y = (int) display_size_y / 2 - d / 2;

            int rand1 = (int) Math.floor(Math.random() * 3);
            int rand2 = (int) Math.floor(Math.random() * 3);

            switch (rand1) {
                default:
                    x_vel = max_speed / 1.5;
                    break;
                case 0:
                    x_vel = max_speed / 1.5;
                    break;
                case 1:
                    x_vel = -max_speed / 1.5;
                    break;
            }
            switch (rand2) {
                default:
                    y_vel = max_speed / 1.5;
                    break;
                case 0:
                    y_vel = max_speed / 1.5;
                    break;
                case 1:
                    y_vel = -max_speed / 1.5;
                    break;
            }
        }
    }

}

