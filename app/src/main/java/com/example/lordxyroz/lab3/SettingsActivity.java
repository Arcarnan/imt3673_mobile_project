package com.example.lordxyroz.lab3;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity
{
    // const
    private static final String PREF_NAME = "name";
    private static final String USER_PREFS = "usernamePreference";
    private static final String PREF_DARK_THEME = "darkmodeState";

    //  external functions
    SetLanguage setLanguage;
    darkmodeStartupClass darkmode;

    //  shared preferences
    SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Darkmode  need to be before super.onCreate, in order to override the default theme
        darkmode = new darkmodeStartupClass();
        setLanguage = new SetLanguage();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //  startup functions
        displayUsername();
        toggleDarkmode();
        selectLanguage();
        buttonPressed();
    }

    private void displayUsername()
    {
        mSharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        String name = mSharedPreferences.getString(PREF_NAME, "");

        TextView username = findViewById(R.id.textView8UsernameDisplay);
        username.setText(name);
    }

    private void selectLanguage()
    {
        Spinner language;
        language = findViewById(R.id.spinnerLanguage);

        //  list for dropdown choices
        List<String> categories = new ArrayList<>();
        categories.add("English");
        categories.add("Norsk");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language.setAdapter(dataAdapter);

        final SharedPreferences languagePreference = PreferenceManager.getDefaultSharedPreferences(this);
        final int L1 = languagePreference.getInt("L1", -1);
        if (L1 != -1)
        {
            language.setSelection(L1);
        }

        language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {

                if (pos == 0 && pos != L1)
                {

                    Toast.makeText(parent.getContext(),
                            "You have selected English", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("en");

                }
                else if (pos == 1 && pos != L1)
                {

                    Toast.makeText(parent.getContext(),
                            "Du har valgt Norsk", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("no");
                }

            }

            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub
            }

        });
    }

    private void buttonPressed()
    {
        final View androidTransitionEpicenter = findViewById(R.id.activity_settings);

        Button mBackButton;
        mBackButton = findViewById(R.id.buttonBackSettings);

        mBackButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                final SharedPreferences toggleDarkMode = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
                final SharedPreferences.Editor darkmodeEditor = toggleDarkMode.edit();
                final Switch darkmode = findViewById(R.id.switch1DarkMode);
                darkmodeEditor.putBoolean(PREF_DARK_THEME, darkmode.isChecked());
                darkmodeEditor.apply();

                //start new activity on return in order to apply changes    //TODO improve method
                //  smooth fade transition
                Intent intent = new Intent(SettingsActivity.this, MenuActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SettingsActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });
    }

    private void toggleDarkmode()
    {
        final Switch darkmodeSwitch = findViewById(R.id.switch1DarkMode);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        boolean darkMode = settings.getBoolean(PREF_DARK_THEME, false);

        //sets the switch of darkmode to the right true/false position
        darkmodeSwitch.setChecked(darkMode);

        darkmodeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked)
            {
                toggleTheme(isChecked);
            }
        });
    }

    private void toggleTheme(boolean darkTheme)
    {
        final View androidTransitionEpicenter = findViewById(R.id.activity_settings);
        final SharedPreferences toggleDarkMode = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        SharedPreferences.Editor editor = toggleDarkMode.edit();
        editor.putBoolean(PREF_DARK_THEME, darkTheme);
        editor.apply();

        //  smooth fade transition
        Intent intent = getIntent();
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, androidTransitionEpicenter, "transition");
        startActivity(intent, options.toBundle());
    }

    public void setLocale(String lang)
    {
        final View androidTransitionEpicenter = findViewById(R.id.activity_settings);
        setLanguage.setLocale(lang, this);

        //  smooth fade transition
        Intent refresh = new Intent(this, this.getClass());
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, androidTransitionEpicenter, "transition");
        startActivity(refresh, options.toBundle());
    }

    @Override
    public void onBackPressed()
    {
        final View androidTransitionEpicenter = findViewById(R.id.activity_settings);

        //  smooth fade transition
        Intent intent = new Intent(SettingsActivity.this, MenuActivity.class);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SettingsActivity.this, androidTransitionEpicenter, "transition");
        startActivity(intent, options.toBundle());
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        final SharedPreferences languagePreference = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = languagePreference.edit();
        final Spinner language = findViewById(R.id.spinnerLanguage);
        final int L1 = language.getSelectedItemPosition();
        editor.putInt("L1", L1);
        editor.apply();
    }
}

