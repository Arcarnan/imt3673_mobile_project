package com.example.lordxyroz.lab3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import java.util.Locale;

public class SetLanguage
{
    public void start(Context context)
    {
        final SharedPreferences languagePreference = PreferenceManager.getDefaultSharedPreferences(context);
        final int L1 = languagePreference.getInt("L1", -1);
        if (L1 == 0 && (!Locale.getDefault().getDisplayLanguage().equals("English")))
        {
            //  if english selected in settings, and english is not set as default language
            setLocale("en", context);
        }
        else if (L1 == 1 && (!Locale.getDefault().getDisplayLanguage().equals("norsk")))
        {
            //  if norwegian is selected in settings, and norwegian is not set to the default language
            setLocale("no", context);
        }
    }

    public void setLocale(String lang, Context context)
    {

        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);

        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(myLocale);
        res.updateConfiguration(conf, dm);
        context.createConfigurationContext(conf);

        Intent refresh = new Intent(context, context.getClass());
        context.startActivity(refresh);
    }
}
