package com.example.lordxyroz.lab3;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class HighscoreActivity extends AppCompatActivity
{
    //  consts
    private static final String PREF_DARK_THEME = "darkmodeState";

    // Listview
    private ListView listView;
    ArrayAdapter<String> arrayAdapterScore;
    ArrayList<String> scores;

    // External classes
    darkmodeStartupClass darkmode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //  need to be before super.onCreate, in order to override the default theme
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        listView = findViewById(R.id.listView);
        scores = new ArrayList<>();

        //  startup function(s)
        buttonPressed();
        getHighscores();

    }


    private void buttonPressed()
    {

        Button mBackButton;
        //  button binding(s)
        mBackButton = findViewById(R.id.buttonBackHighscore);

        mBackButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                //  finish activity after animation is played
                finishAfterTransition();
            }
        });
    }

    private void getHighscores()
    {
        //  gets the highscore list from firebase
        final String TAG = "highscores";
        FirebaseFirestore.getInstance().collection(TAG)
                //.orderBy("score")
                .orderBy("score", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>()
                {
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e)
                    {
                        if (e != null)
                        {
                            Log.w(TAG, "Listen failed");
                        } else {
                            int counter = 0;
                            scores.clear();
                            for (DocumentSnapshot doc : documentSnapshots)
                            {
                                if (counter <= 10)
                                {
                                    if (doc.get("score") != null)
                                    {
                                        String name = doc.getString("name");
                                        String score = doc.get("score").toString();
                                        scores.add(name + ": " + score);
                                        counter++;
                                        Log.d(TAG, "Getting data works fine");

                                        arrayAdapterScore = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, scores);
                                        listView.setAdapter(arrayAdapterScore);

                                    } else
                                        {
                                        Log.w(TAG, "No value reading documents");
                                    }
                                }
                            }
                        }
                    }
                });
    }
}
