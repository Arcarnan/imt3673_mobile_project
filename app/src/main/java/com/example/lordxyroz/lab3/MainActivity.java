package com.example.lordxyroz.lab3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.util.Locale;

public class MainActivity extends Activity
{
    //  consts
    final String PREF_DARK_THEME = "darkmodeState";

    //  external classes
    protected GameView gameView;
    darkmodeStartupClass darkmode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //  Darkmode need to be before super.onCreate, in order to override the standard theme
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(0xFFFFFFFF,WindowManager.LayoutParams.FLAG_FULLSCREEN| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        gameView = new GameView(this, getWindowManager().getDefaultDisplay());
        setContentView(gameView);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        gameView.resume();
    }
}
