package com.example.lordxyroz.lab3;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class DebugChatActivity extends AppCompatActivity
{
    darkmodeStartupClass darkmode;
    final String PREF_DARK_THEME = "darkmodeState";

    private Button mSendButton;
    private Button mConnectButton;
    private ListView listviewMessages;
    private ArrayList<String> messageArray;

    private TextToSpeech tts = null;

    EditText mEdit;

    //////////////////////////////////////////////////////////////////////////////////////
    // P2P

    private DatagramSocket socket;
    final int port = 9999;
    InetAddress host = null;

    boolean isClient;

    final byte receiveData[] = new byte[512];

    // Thread
    private Runnable runnable;
    private Thread thread;

    /////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //need to be before super.onCreate
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_chat);

        listviewMessages = findViewById(R.id.listViewMessages);
        messageArray = new ArrayList<String>();
        mEdit = findViewById(R.id.textMessage);
        mSendButton = findViewById(R.id.buttonSendMessage);

        Intent intent = getIntent();

        isClient = intent.getBooleanExtra("isClient", false);

        if (isClient) {
            try {
                host = InetAddress.getByName(intent.getStringExtra("address"));
            } catch (IOException exc) {
                Log.w("EEEEE", exc.getMessage());
            }

            runnable = new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        Log.e("AAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAA");
                        try {
                            if (socket == null) {
                                socket = new DatagramSocket(port);
                                socket.setSoTimeout(1000000000);
                                socket.connect(host, port);
                            }
                        } catch (IOException exc) {
                            Log.e("P2P Client", exc.getMessage());
                        }

                        try {
                            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            socket.receive(receivePacket);
                            String received = new String(receivePacket.getData(), 0, receivePacket.getLength());
                            speak(received);
                            messageArray.add(received);
                            Log.e("P2P Client receive", received);
                        } catch (IOException exc) {
                            Log.e("P2P Client", exc.getMessage());
                        } catch (NullPointerException exc) {
                            Log.e("P2P Client", exc.getMessage());
                        }

                        final ArrayAdapter<String> adapter = new ArrayAdapter<>(DebugChatActivity.this, android.R.layout.simple_list_item_1, messageArray);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listviewMessages.setAdapter(adapter);
                                listviewMessages.setSelection(adapter.getCount() - 1);
                            }
                        });

                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                    Log.e("Client THREAD", "Stopped");
                }
            };

            thread = new Thread(runnable);
            thread.start();

            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    byte sendData[] = new byte[512];
                    String message = mEdit.getText().toString();
                    try {
                        sendData = (message).getBytes();
                        DatagramPacket packet = new DatagramPacket(sendData, sendData.length, host, port);
                        socket.send(packet);
                        Log.e("P2P Client", host.toString());
                        messageArray.add(new String(sendData));
                    } catch (IOException exc) {
                        Log.e("P2P Client", exc.getMessage());
                    }

                    final ArrayAdapter<String> adapter = new ArrayAdapter<>(DebugChatActivity.this, android.R.layout.simple_list_item_1, messageArray);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listviewMessages.setAdapter(adapter);
                            listviewMessages.setSelection(adapter.getCount() - 1);
                        }
                    });
                }
            });
        } else {
            runnable = new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        Log.e("BBBBBBBBBBBBBBBBBBB", "AAAAAAAAAAAAAAAAAAAA");
                        try {
                            if (socket == null) {
                                socket = new DatagramSocket(port);
                                socket.setSoTimeout(1000000000);
                            }
                        } catch (IOException exc) {
                            Log.e("P2P Host", exc.getMessage());
                        }

                        try {
                            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                            socket.receive(receivePacket);
                            String received = new String(receivePacket.getData(), 0, receivePacket.getLength());
                            speak(received);
                            messageArray.add(received);
                            Log.e("P2P Host receive", received);
                            Log.e("P2P Host", receivePacket.getAddress().toString());

                            if (host == null) {
                                host = receivePacket.getAddress();
                                socket.connect(host, port);
                            }

                        } catch (IOException exc) {
                            Log.e("P2P Host", exc.getMessage());
                        } catch (NullPointerException exc) {
                            Log.e("P2P Host", exc.getMessage());
                        }

                        final ArrayAdapter<String> adapter = new ArrayAdapter<>(DebugChatActivity.this, android.R.layout.simple_list_item_1, messageArray);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listviewMessages.setAdapter(adapter);
                                listviewMessages.setSelection(adapter.getCount() - 1);
                            }
                        });

                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                    Log.e("HOST THREAD", "Stopped");
                }
            };

            thread = new Thread(runnable);
            thread.start();

            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (host != null) {
                        String message = mEdit.getText().toString();
                        byte sendData[] = new byte[512];
                        try {
                            sendData = (message).getBytes();
                            DatagramPacket packet = new DatagramPacket(sendData, sendData.length, host, port);
                            socket.send(packet);
                            messageArray.add(new String(sendData));
                            Log.e("P2P Host", host.toString());

                            //clear message field after message is sent
                            mEdit.getText().clear();

                        } catch (IOException exc) {
                            Log.e("P2P Host", exc.getMessage());
                        }
                    }

                    final ArrayAdapter<String> adapter = new ArrayAdapter<>(DebugChatActivity.this, android.R.layout.simple_list_item_1, messageArray);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listviewMessages.setAdapter(adapter);
                            listviewMessages.setSelection(adapter.getCount() - 1);
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onStop() {
        socket.close();
        try {
            thread.interrupt();
            thread.join();
        }
        catch (Exception exc) {

        }
        super.onStop();
        if (tts != null) {
            tts.shutdown();
        }
        finish();
    }

    public void speak(final String str) {
        if (tts != null) {
            tts.shutdown();
        }
        tts = new TextToSpeech(DebugChatActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.JAPANESE);
                    tts.speak(str, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });
    }

    @Override
    public void onBackPressed(){
        socket.close();
        try {
            thread.interrupt();
            thread.join();
        }
        catch (Exception exc) {

        }
        if (tts != null) {
            tts.shutdown();
        }
        finish();
    }
}
