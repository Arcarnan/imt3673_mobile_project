package com.example.lordxyroz.lab3;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class darkmodeStartupClass
{
    public void darkmodeStartup(Context context, String PREF_DARK_THEME)
    {
        SharedPreferences PREFS_NAME = PreferenceManager.getDefaultSharedPreferences(context);
        boolean darkModeStart = PREFS_NAME.getBoolean(PREF_DARK_THEME, false);

        //  sets the theme to the Dark theme created in colors.xml or the standard theme based on what is saved in sharedPreference
        if (darkModeStart)
        {
            context.setTheme(R.style.AppTheme_Dark_NoActionBar);
        }
        else
        {
            context.setTheme((R.style.AppTheme_NoActionBar));
        }
    }
}
