package com.example.lordxyroz.lab3;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Locale;

public class DebugChatActivity2 extends AppCompatActivity
{
    darkmodeStartupClass darkmode;
    final String PREF_DARK_THEME = "darkmodeState";

    private TextToSpeech tts = null;

    private Button mSendButton;
    private ListView listviewMessages;
    private ArrayList<String> messageArray;

    EditText mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //need to be before super.onCreate
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_chat2);


        listviewMessages = findViewById(R.id.listViewMessages2);
        messageArray = new ArrayList<String>();
        mEdit = findViewById(R.id.textMessage2);
        mSendButton = findViewById(R.id.buttonSendMessage2);

        //  button for changing activity to settings
        mSendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String message = mEdit.getText().toString();
                messageArray.add(message);
                speak(message);

                //clear message field after message is sent
                mEdit.getText().clear();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(DebugChatActivity2.this, android.R.layout.simple_list_item_1, messageArray);
        listviewMessages.setSelection(adapter.getCount() - 1);
        listviewMessages.setAdapter(adapter);
    }

    public void speak(final String str) {
        if (tts != null) {
            tts.shutdown();
        }
        tts = new TextToSpeech(DebugChatActivity2.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.JAPANESE);
                    tts.speak(str, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });
    }

    @Override
    public void onStop()
    {
        super.onStop();
        if (tts != null) {
            tts.shutdown();
        }
        finish();
    }
}
