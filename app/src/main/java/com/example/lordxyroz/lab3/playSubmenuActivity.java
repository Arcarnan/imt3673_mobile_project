package com.example.lordxyroz.lab3;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.View;
import android.widget.Button;

public class playSubmenuActivity extends AppCompatActivity
{
    //  consts
    final String PREF_DARK_THEME = "darkmodeState";

    //  external classes
    darkmodeStartupClass darkmode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //  need to be before super.onCreate, in order to override the default theme
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_submenu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //  startup function(s)
        buttonPressed();

        //  animation, plays individual animation instead of the shared one when activity starts
        getWindow().setEnterTransition(new Slide());
    }

    //function for button pressed from main menu, moved out of onCreate
    private void buttonPressed ()
    {
        Button mAIButton;
        Button mJoinButton;
        Button mHostButton;
        Button mDebugChatButton;
        Button mDebugChatButton2;
        Button mQuitButton;

        //  button bindings
        mAIButton = findViewById(R.id.button1AI);
        mJoinButton = findViewById(R.id.button2JoinGame);
        mHostButton = findViewById(R.id.button3HostGame);
        mDebugChatButton = findViewById(R.id.button4ChatDebug);
        mDebugChatButton2 = findViewById(R.id.button4ChatDebug2);
        mQuitButton = findViewById(R.id.buttonBackSubmenu);

        // get the common element for the transition in this activity
        final View androidTransitionEpicenter = findViewById(R.id.activity_play_submenu);

        //Starts game with AI
        mAIButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(playSubmenuActivity.this, MainActivity.class));

                ////////////////    For some reason, smooth transition uses a long time to enter game   /////////////
                //  smooth fade transition
                //Intent intent = new Intent(playSubmenuActivity.this, MainActivity.class);
                //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(playSubmenuActivity.this, androidTransitionEpicenter, "transition");
                //startActivity(intent, options.toBundle());
            }
        });

        //Search for game to join
        mJoinButton.setOnClickListener(new View.OnClickListener()
        {
            //TODO change target new activity
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(playSubmenuActivity.this, Temp.class));

                ////////////////    For some reason, smooth transition uses a long time to enter game   /////////////
                //  smooth fade transition
                //Intent intent = new Intent(playSubmenuActivity.this, MainActivity.class);
                //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(playSubmenuActivity.this, androidTransitionEpicenter, "transition");
                //startActivity(intent, options.toBundle());
            }
        });

        //Starts hosting a game
        mHostButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //TODO change target new activity
                //startActivity(new Intent(playSubmenuActivity.this, Temp.class));

                ////////////////    For some reason, smooth transition uses a long time to enter game   /////////////
                //  smooth fade transition
                Intent intent = new Intent(playSubmenuActivity.this, Temp.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(playSubmenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });

        //Debug chat for networking purposes
        mDebugChatButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //TODO change target new activity
                Intent intent = new Intent(playSubmenuActivity.this, Temp.class);
                intent.putExtra("chat", true);
                //startActivity(intent);

                //  smooth fade transition
                //Intent intent = new Intent(playSubmenuActivity.this, Temp.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(playSubmenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });

        //Debug chat 2 for networking purposes
        mDebugChatButton2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //TODO change target new activity
                //  smooth fade transition
                Intent intent = new Intent(playSubmenuActivity.this, DebugChatActivity2.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(playSubmenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });

        //Moves back to main menu
        mQuitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finishAfterTransition();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        //  finish activity after animation is played
        finishAfterTransition();
    }
}
