package com.example.lordxyroz.lab3;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DiscretePathEffect;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Locale;
import java.util.Map;

public class GameView extends SurfaceView implements Runnable, SensorEventListener2 {

    final String PREF_DARK_THEME = "darkmodeState";

    private boolean isRunning = false;

    private Thread gameThread;
    private SurfaceHolder holder;
    private Vibrator haptic;
    private ToneGenerator sound;

    float display_size_x;
    float display_size_y;

    private Player player;
    private Opponent opponent;
    private Ball ball;

    private SensorManager sensorManager;

    private float accelerometer[] = new float[3];
    private float magnetic[] = new float[3];
    private float rotation[] = new float[9];
    private float radians[] = new float[3];

    private double gravity = 9.81;
    private double max_speed = 15;
    private double last_update;

    private boolean darkmode;

    TextToSpeech tts = null;


    //Constructor
    public GameView(Context context, Display display) {
        super(context);
        darkmodeStartup();
        holder = getHolder();
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Point size = new Point();
        display.getSize(size);
        display_size_x = size.x;
        display_size_y = size.y;

        haptic = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        player = new Player();
        opponent = new Opponent();
        ball = new Ball();
    }

    @Override
    public void run() {
        while (isRunning) {
            if (!holder.getSurface().isValid()) {
                continue;
            }

            // update
            double now = System.nanoTime() / 10000000.d;
            double delta_time = now - last_update;

            //Log.d("delta_time", Double.toString(delta_time));
            if (delta_time > 1 / 60d) {
                player.update(delta_time);
                opponent.update(delta_time);
                ball.update();
                last_update = now;

                // draw
                Canvas canvas = holder.lockCanvas();
                if (canvas != null) {
                    if (darkmode)
                    {
                        canvas.drawColor(Color.DKGRAY);
                    }
                    else
                    {
                        canvas.drawColor(Color.LTGRAY);
                    }
                    player.draw(canvas);
                    opponent.draw(canvas);
                    ball.draw(canvas);
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void resume() {
        isRunning = true;
        gameThread = new Thread(this);
        gameThread.start();

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void speak(final String str) {
        if (tts != null) {
            tts.shutdown();
        }
        tts = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.JAPANESE);
                    tts.speak(str, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });
    }

    public void pause() {
        isRunning = false;
        if (tts != null) tts.shutdown();
        boolean retry = true;
        while (retry) {
            try {
                gameThread.join();
                retry = false;
                sensorManager.unregisterListener(this);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometer[0] = sensorEvent.values[0];
            accelerometer[1] = sensorEvent.values[1];
            accelerometer[2] = sensorEvent.values[2];
        }
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetic[0] = sensorEvent.values[0];
            magnetic[1] = sensorEvent.values[1];
            magnetic[2] = sensorEvent.values[2];
        }

        SensorManager.getRotationMatrix(rotation, null, accelerometer, magnetic);
        SensorManager.getOrientation(rotation, radians);
    }

    @Override
    public void onFlushCompleted(Sensor sensor){}

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    public class Player {
        private ShapeDrawable paddle;

        private int x;
        private int y;
        private int d = 100;

        private double y_vel;

        public Player() {
            paddle = new ShapeDrawable(new RectShape());
            paddle.getPaint().setColor(Color.RED);

            x = (int) display_size_x - 200;
            y = (int) display_size_y / 2 - d / 2;

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
        }

        public void update(final double delta_time) {
            y_vel = y_vel + (Math.sin(radians[2]) * gravity * delta_time);

            if (y_vel > max_speed) y_vel = max_speed;
            if (y_vel < -max_speed) y_vel =-max_speed;

            if (y - y_vel <= (d / 4) + (d * 2)|| y - y_vel >= display_size_y - (d * 2) - (d / 4)){
                y_vel = 0;
            }

            if (y_vel < 0.001 && y_vel > -0.001) y_vel = 0;

            y -= y_vel;
            y = ball.y;
        }

        protected void draw(Canvas canvas) {

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
            paddle.draw(canvas);
        }
    }

    public class Opponent {
        private ShapeDrawable paddle;

        private int x;
        private int y;
        private int d = 100;

        private double y_vel;

        public Opponent() {
            paddle = new ShapeDrawable(new RectShape());
            paddle.getPaint().setColor(Color.RED);

            x = 200;
            y = (int) display_size_y / 2 - d / 2;

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
        }

        // TODO: Change this to AI or controlled by another device
        public void update(final double delta_time) {
            y_vel = y_vel + (Math.sin(radians[2]) * gravity * delta_time);

            if (y_vel > max_speed) y_vel = max_speed;
            if (y_vel < -max_speed) y_vel =-max_speed;

            if (y - y_vel <= (d / 4) || y - y_vel >= display_size_y - (d * 2) - (d / 4)){
                y_vel = 0;
            }

            if (y_vel < 0.001 && y_vel > -0.001) y_vel = 0;

            y -= y_vel;
            y = ball.y;
        }

        protected void draw(Canvas canvas) {

            paddle.setBounds(x - (d / 4), y - (d * 2), x + (d / 4), y + (d * 2));
            paddle.draw(canvas);
        }
    }

    public class Ball {
        private ShapeDrawable ball;

        private int x;
        private int y;
        private int d = 100;

        private double x_vel;
        private double y_vel;

        public Ball() {
            ball = new ShapeDrawable(new OvalShape());
            ball.getPaint().setColor(Color.RED);

            x = (int) display_size_x / 2 - d / 2;
            y = (int) display_size_y / 2 - d / 2;

            ball.setBounds(x - (d / 2), y - (d / 2), x + (d / 2), y + (d / 2));

            int rand1 = (int) Math.floor(Math.random() * 3);
            int rand2 = (int) Math.floor(Math.random() * 3);
            switch (rand1) {
                default:
                    x_vel = max_speed / 1.5;
                    break;
                case 0:
                    x_vel = max_speed / 1.5;
                    break;
                case 1:
                    x_vel = -max_speed / 1.5;
                    break;
            }
            switch (rand2) {
                default:
                    y_vel = max_speed / 1.5;
                    break;
                case 0:
                    y_vel = max_speed / 1.5;
                    break;
                case 1:
                    y_vel = -max_speed / 1.5;
                    break;
            }
        }

        public void update() {
            if (x_vel > max_speed * 1.5) x_vel = max_speed * 1.5;
            if (x_vel < -max_speed * 1.5) x_vel = -max_speed * 1.5;
            if (y_vel > max_speed * 1.5) y_vel = max_speed * 1.5;
            if (y_vel < -max_speed * 1.5) y_vel =-max_speed * 1.5;

            // TODO: Real pong collision
            if (x + (d / 2) >= player.x - (d / 4)) {
                if (y >= player.y - (d * 2) && y <= player.y + (d * 2)) {
                    x_vel = -x_vel;
                }
            }

            if (x - (d / 2) <= opponent.x + (d / 4)) {
                if (y >= opponent.y - (d * 2) && y <= opponent.y + (d * 2)) {
                    x_vel = -x_vel;
                }
            }

            // Bounce
            if (x - x_vel <= (d / 2) || x - x_vel >= display_size_x - (d / 2)) {
                x_vel = -x_vel * 1.1d;
            }
            if (y - y_vel <= (d / 2) || y - y_vel >= display_size_y - (d / 2)){
                y_vel = -y_vel * 1.1d;
            }

            x -= x_vel;
            y -= y_vel;

        }

        public void draw(Canvas canvas) {
            ball.setBounds(x - (d / 2), y - (d / 2), x + (d / 2), y + (d / 2));
            ball.draw(canvas);
        }

        public float map(double value, double start1, double stop1, double start2, double stop2) {
            return (float) (((value - start1) / (stop1 - start1)) * (stop2 - start2) + start2);
        }

    }


    //TODO need to find a way to change theme in context when no onCreate exists
    public void darkmodeStartup()
    {
        SharedPreferences PREFS_NAME = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean darkModeStart = PREFS_NAME.getBoolean(PREF_DARK_THEME, false);

        darkmode = darkModeStart;
    }
}
