package com.example.lordxyroz.lab3;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Scene;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.plattysoft.leonids.ParticleSystem;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MenuActivity extends AppCompatActivity
{
    // external functions
    darkmodeStartupClass darkmode;
    SetLanguage setLanguage;

    // const
    private static final String PREF_DARK_THEME = "darkmodeState";
    private static final String PREF_NAME = "name";
    private static final String USER_PREFS = "usernamePreference";

    // shared preferences
    SharedPreferences mSharedPreferences;

    // Particle emitter
    private View emiter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //  darkmode need to be before super.onCreate, in order to override the default theme
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        emiter = findViewById(R.id.emiter);

        // Set language
        setLanguage = new SetLanguage();
        setLanguage.start(this);

        // Sets the user name/welcomes the user.
        displayWelcome();

        // startup function(s)
        buttonPressed();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Set position of particle system.
        emiter.setX(event.getX());
        emiter.setY(event.getY());

        new ParticleSystem(this, 15, R.drawable.animated_confetti, 2000)
                .setSpeedModuleAndAngleRange(0.04f, 0.10f, 0, 360)
                .setRotationSpeed(150)
                .setAcceleration(0.000020f, 0)
                .oneShot(emiter, 5);

        return super.onTouchEvent(event);
    }

    //function for button pressed from main menu, moved out of onCreate
    private void buttonPressed ()
    {
        // Buttons
        Button mPlayButton;
        Button mHighscoreButton;
        Button mSettingsButton;
        Button mCreditsButton;
        Button mQuitButton;

        //  button binding(s)
        mPlayButton = findViewById(R.id.button1Play);
        mHighscoreButton = findViewById(R.id.button2Highscore);
        mSettingsButton = findViewById(R.id.button3Settings);
        mCreditsButton = findViewById(R.id.button4Credits);
        mQuitButton = findViewById(R.id.button5Quit);

        // get the common element for the transition in this activity
        final View androidTransitionEpicenter = findViewById(R.id.activity_menu);

        //Starts corresponding activity based on what button is pressed
        mPlayButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // getWindow().setExitTransition(new Explode());

                //  smooth fade transition
                Intent intent = new Intent(MenuActivity.this, playSubmenuActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });



       mHighscoreButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                ////////////////////////////////////////////////////////////////////
                //        smooth fade transition
                Intent intent = new Intent(MenuActivity.this, HighscoreActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
                /////////////////////////////////////////////////////////////////////


                /////////////////////////////////////////////////////////////////////
                //        used for transitions that are not fade, affects the shared transition
                //Intent intent = new Intent(MenuActivity.this, HighscoreActivity.class);
                //getWindow().setExitTransition(new Slide());
                //startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(MenuActivity.this).toBundle());
                /////////////////////////////////////////////////////////////////////
            }
        });



        mSettingsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                //  smooth fade transition
                Intent intent = new Intent(MenuActivity.this, SettingsActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });

        mCreditsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                //  smooth fade transition
                Intent intent = new Intent(MenuActivity.this, CreditsActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MenuActivity.this, androidTransitionEpicenter, "transition");
                startActivity(intent, options.toBundle());
            }
        });

        //creates an alert window if quit is pressed, asking the user if they really want to quit
        mQuitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                new AlertDialog.Builder(MenuActivity.this)
                        .setTitle(R.string.textExitApplicationTitle)
                        .setMessage(R.string.textExitApplication)
                        .setNegativeButton(R.string.textNo, null)
                        .setPositiveButton(R.string.textYes, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface arg0, int arg1)
                            {
                                //A1.super.onBackPressed();
                                finishAffinity();
                            }
                        }).create().show();
            }
        });
    }

    //  //creates an alert window if the back button is pressed from this activity, asking the user if they really want to quit
    @Override
    public void onBackPressed()
    {
        //Shows option to exit application if back is pressed from main screen
        new AlertDialog.Builder(this)
                .setTitle(R.string.textExitApplicationTitle)
                .setMessage(R.string.textExitApplication)
                .setNegativeButton(R.string.textNo, null)
                .setPositiveButton(R.string.textYes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        //A1.super.onBackPressed();
                        finishAffinity();
                    }
                }).create().show();
    }

    public void displayWelcome() {
        mSharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);

        String name = mSharedPreferences.getString(PREF_NAME, "");

        // If user already has a name
        if (name.length() > 0)
        {
            // TODO need to add text to speech on this one xD   -   called every time we return to mainMenu due to the settings implementation
            //Toast.makeText(this, "Welcome back, " + name + "!", Toast.LENGTH_LONG).show();
        }
        else
            {
            // Show a dialog to ask for their name
            final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(this);
            alert.setTitle(R.string.textHello);
            alert.setMessage(R.string.textWhatIsUsername);

            // Create EditText for entry
            final EditText input = new EditText(this);
            alert.setView(input);

            // Make an "OK" button to save the name
            alert.setPositiveButton(R.string.textOk, new DialogInterface.OnClickListener()
            {

                public void onClick(DialogInterface dialog, int whichButton)
                {

                    // Grab the EditText's input
                    final String inputName = input.getText().toString();

                    if (inputName.length() < 16)
                    {
                        // Save the name in app:
                        SharedPreferences.Editor e = mSharedPreferences.edit();
                        e.putString(PREF_NAME, inputName);
                        e.apply();

                        // Welcome the new user
                        String  welcomeText = getApplicationContext().getString(R.string.textWelcome);
                        Toast.makeText(getApplicationContext(), welcomeText + ", " + inputName + "!", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), R.string.textNameTooLong, Toast.LENGTH_LONG).show();
                        displayWelcome();
                    }

                }
            });

            // A cancel button that closes the app, no name - no game!
            alert.setNegativeButton(R.string.textCancel, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {
                    finishAfterTransition();
                }
            });

            // Closes app if user tries to bypass naming!
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finishAfterTransition();
                }
            });
            alert.show();
        }
    }
}

