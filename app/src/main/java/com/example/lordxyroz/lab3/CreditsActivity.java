package com.example.lordxyroz.lab3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CreditsActivity extends AppCompatActivity
{
    final String PREF_DARK_THEME = "darkmodeState";

    darkmodeStartupClass darkmode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //need to be before super.onCreate
        darkmode = new darkmodeStartupClass();
        darkmode.darkmodeStartup(this, PREF_DARK_THEME);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);

        //  startup function(s)
        buttonPressed();
    }

    private void buttonPressed()
    {
        Button mBackButton;

        //  button binding(s)
        mBackButton = findViewById(R.id.buttonBackCredits);

        mBackButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finishAfterTransition();
            }
        });
    }
}
